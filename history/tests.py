import json
import time
from datetime import datetime
from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.test import TestCase
import pytz
from rest_framework import status
from rest_framework.test import APIClient
from history.models import History, validate_quiz_answer
from history.serializers import HistorySerializer
from potay_webservice.test_utils import generate_test_token

tz = pytz.timezone(settings.TIME_ZONE)


class HistoryModelUnitTest(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = User.objects.create_user("a", "a@a.com", "a")
        cls.quiz_answer_raw = [1, 3, 5, 7, 8]
        cls.quiz_answer = json.dumps(cls.quiz_answer_raw)

    def setUp(self):
        self.valid_history = History(user=self.user,
                                     confident=True,
                                     texture="Normal",
                                     color="Brown",
                                     quiz_answer=self.quiz_answer)

    def test_pos_create_history_object(self):
        self.valid_history.full_clean()
        self.valid_history.save()
        self.assertEqual(History.objects.count(), 1)

    def test_neg_create_history_object(self):
        history1 = History(user=self.user,
                           texture="Normal",
                           color="Brown",
                           quiz_answer=self.quiz_answer)
        with self.assertRaises(ValidationError):
            history1.full_clean()
        history2 = History(user=self.user,
                           texture="Normal",
                           color="Brown",
                           quiz_answer=self.quiz_answer)
        with self.assertRaises(ValidationError):
            history2.full_clean()
        history3 = History(user=self.user,
                           texture="Normal",
                           color="Brown",
                           quiz_answer="meng")
        with self.assertRaises(ValidationError):
            history3.full_clean()

    def test_get_list_from_quiz_answer(self):
        self.assertEqual(self.valid_history.quiz_answer_list,
                         self.quiz_answer_raw)


class HistoryValidatorsUnitTest(TestCase):
    def test_pos_validate_quiz_answer(self):
        self.assertIsNone(validate_quiz_answer("[]"))
        self.assertIsNone(validate_quiz_answer("[1, 2, 3]"))

    def test_neg_validate_quiz_answer(self):
        with self.assertRaises(ValidationError):
            validate_quiz_answer([1, 2, 3])
        with self.assertRaises(ValidationError):
            validate_quiz_answer("")
        with self.assertRaises(ValidationError):
            validate_quiz_answer("meng")
        with self.assertRaises(ValidationError):
            validate_quiz_answer("[\"hehe\", 1, 2]")


class HistorySerializerUnitTest(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.quiz_answer_raw = [1, 2, 3]
        user = User.objects.create_user("a", "a@a.com", "a")
        quiz_answer = json.dumps(cls.quiz_answer_raw)
        cls.data = {
            "user": user,
            "confident": True,
            "texture": "Normal",
            "color": "Brown",
            "quiz_answer": quiz_answer,
        }
        cls.history = History.objects.create(**cls.data)

    def test_history_serializer(self):
        serialized = HistorySerializer(instance=self.history)
        self.assertEqual(
            set(serialized.data.keys()),
            set([
                'id', 'timestamp', 'confident', 'texture', 'color',
                'quiz_answer'
            ]))
        self.assertEqual(datetime.fromisoformat(serialized.data["timestamp"]),
                         self.history.timestamp)
        self.assertEqual(serialized.data["id"], self.history.pk)
        self.assertEqual(serialized.data["confident"], self.data["confident"])
        self.assertEqual(serialized.data["texture"], self.data["texture"])
        self.assertEqual(serialized.data["color"], self.data["color"])
        self.assertEqual(serialized.data["quiz_answer"], self.quiz_answer_raw)


class HistoryAPIUnitTest(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.url = "/history/"
        cls.user1 = User.objects.create_user("a", "a@a.com", "a")
        cls.user2 = User.objects.create_user("b", "b@b.com", "b")
        codes1 = json.dumps([1, 2, 3, 4, 6])
        codes2 = json.dumps([5, 7, 8, 9, 10])
        cls.history1 = History.objects.create(user=cls.user1,
                                              confident=True,
                                              texture="Constipated",
                                              color="Blood",
                                              quiz_answer=codes1)
        cls.history2 = History.objects.create(user=cls.user1,
                                              confident=False,
                                              texture="Normal",
                                              color="Green",
                                              quiz_answer=codes2)
        cls.history3 = History.objects.create(user=cls.user2,
                                              confident=True,
                                              texture="Diarrhea",
                                              color="White",
                                              quiz_answer=codes1)

    def test_reg_add_history_item_success(self):
        # Positive test: registered user, add history item valid and success
        header = generate_test_token(user=self.user2, password="b")
        quiz_answer = json.dumps([1, 3, 5, 6, 7])
        payload = {
            "confident": True,
            "texture": "Normal",
            "color": "Brown",
            "quiz_answer": quiz_answer,
        }
        response = APIClient().post(self.url, payload, format='json', **header)
        self.assertTrue(response.data["success"])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(History.objects.count(), 4)
        self.assertEqual(History.objects.filter(user=self.user2).count(), 2)

    def test_reg_add_history_item_ignore_user_timestamp(self):
        # Positive test: registered user, ignoring user's timestamp
        header = generate_test_token(user=self.user2, password="b")
        quiz_answer = json.dumps([1, 3, 5, 6, 7])
        user_timestamp = datetime.now()
        payload = {
            "confident": True,
            "texture": "Constipated",
            "color": "Blood",
            "quiz_answer": quiz_answer,
            "timestamp": user_timestamp.isoformat(),
        }
        time.sleep(0.05)  # Add some intentional delay
        response = APIClient().post(self.url, payload, format='json', **header)
        self.assertTrue(response.data["success"])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(History.objects.count(), 4)
        self.assertEqual(History.objects.filter(user=self.user2).count(), 2)
        obj = History.objects.filter(
            user=self.user2).order_by("-timestamp").first()
        self.assertGreater(obj.timestamp, tz.localize(user_timestamp))

    def test_reg_add_history_item_invalid_input(self):
        # Negative test: registered user, invalid input data
        header = generate_test_token(user=self.user2, password="b")
        payload = {
            "confident": False,
            "texture": "Constipated",
            "color": "Brown",
            "quiz_answer": "",
        }
        response = APIClient().post(self.url, payload, format='json', **header)
        self.assertFalse(response.data["success"])
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(response.data["message"]), 1)
        self.assertEqual(History.objects.count(), 3)
        self.assertEqual(History.objects.filter(user=self.user2).count(), 1)

    def test_pub_add_history_item_fail(self):
        # Negative test: public can't add history items
        quiz_answer = json.dumps([1, 3, 4])
        payload = {
            "confident": True,
            "texture": 1,
            "color": 2,
            "quiz_answer": quiz_answer,
        }
        response = APIClient().post(self.url, payload, format='json')
        self.assertFalse(response.data["success"])
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_reg_clear_all_history_success(self):
        # Positive test: registered user, clear all data always success
        header = generate_test_token(user=self.user1, password="a")
        response = APIClient().delete(self.url, **header)
        self.assertTrue(response.data["success"])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(History.objects.count(), 1)
        self.assertEqual(History.objects.filter(user=self.user1).count(), 0)

    def test_pub_clear_all_history_fail(self):
        # Negative test: public, clear all data always fail
        response = APIClient().delete(self.url)
        self.assertFalse(response.data["success"])
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(History.objects.count(), 3)

    def test_reg_view_all_history_success(self):
        # Positive test: registered user, view history always success
        header = generate_test_token(user=self.user1, password="a")
        response = APIClient().get(self.url, **header)
        self.assertTrue(response.data["success"])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 2)

    def test_pub_view_all_history_fail(self):
        # Negative test: public, view history always fail
        response = APIClient().get(self.url)
        self.assertFalse(response.data["success"])
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_reg_view_history_success(self):
        # Positive test: registered user, view history always success
        header = generate_test_token(user=self.user1, password="a")
        response = APIClient().get(self.url + "?id=%s" % self.history1.pk,
                                   **header)
        self.assertTrue(response.data["success"])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["result"]["color"], self.history1.color)
        self.assertEqual(response.data["result"]["texture"],
                         self.history1.texture)

    def test_pub_view_history_fail(self):
        # Negative test: public, view history always fail
        response = APIClient().get(self.url + "?id=%s" % self.history1.pk)
        self.assertFalse(response.data["success"])
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
