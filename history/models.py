import json
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models


def validate_quiz_answer(value):
    try:
        lst = json.loads(value)
        if not isinstance(lst, list):
            raise ValidationError("Invalid JSON, should be list of integers.")
        if not all(isinstance(x, int) for x in lst):
            raise ValidationError("Invalid JSON, should be list of integers.")
    except (TypeError, json.decoder.JSONDecodeError):
        raise ValidationError("Invalid JSON format, cannot parse.")


# Create your models here.
class History(models.Model):
    # WARNING: Texture and Color choices are still in discussion
    timestamp = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User,
                             null=False,
                             blank=False,
                             related_name="health_records",
                             on_delete=models.CASCADE)
    confident = models.BooleanField()
    texture = models.CharField(max_length=20, null=False, blank=False)
    color = models.CharField(max_length=20, null=False, blank=False)
    quiz_answer = models.TextField(null=False,
                                   blank=False,
                                   validators=[validate_quiz_answer])

    @property
    def quiz_answer_list(self):
        return json.loads(self.quiz_answer)
