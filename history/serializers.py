from rest_framework import serializers
from history.models import History


class HistorySerializer(serializers.ModelSerializer):
    def get_quiz_answer_list(self, obj):
        return obj.quiz_answer_list

    quiz_answer = serializers.SerializerMethodField("get_quiz_answer_list")

    class Meta:
        model = History
        fields = [
            'id', 'timestamp', 'confident', 'texture', 'color', 'quiz_answer'
        ]
