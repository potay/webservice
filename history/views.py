from django.core.exceptions import ValidationError
from rest_framework.decorators import api_view, permission_classes, parser_classes
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.parsers import JSONParser, FormParser
from history.models import History
from history.serializers import HistorySerializer


class HistoryPagination(PageNumberPagination):
    page_size = 10
    page_query_param = 'page'
    page_size_query_param = 'per_page'
    max_page_size = 1000

    def get_paginated_response(self, data):
        return Response({
            'success': True,
            'count': self.page.paginator.count,
            'total_page': self.page.paginator.num_pages,
            'next': self.get_next_link(),
            'previous': self.get_previous_link(),
            'results': data
        })


@api_view(["GET", "POST", "DELETE"])
@permission_classes((IsAuthenticated, ))
@parser_classes([JSONParser, FormParser])
def index(request):
    if request.method == "POST":
        return add_result_to_history(request)
    if request.method == "DELETE":
        return clear_history(request)
    if request.GET.get("id"):
        return get_history_items(request)
    return list_history_items(request)


def list_history_items(request):
    paginator = HistoryPagination()
    items = History.objects.filter(user=request.user).order_by("-timestamp")
    result_page = paginator.paginate_queryset(items, request)
    serialized = HistorySerializer(instance=result_page, many=True).data
    response = paginator.get_paginated_response(serialized)
    return response


def get_history_items(request):
    item = History.objects.get(user=request.user, id=request.GET["id"])
    serialized = HistorySerializer(instance=item).data
    return Response({"success": True, "result": serialized})


def add_result_to_history(request):
    payload = dict(request.data)
    payload.pop("timestamp", None)
    payload.update({"user": request.user})
    history = History(**payload)
    try:
        history.full_clean()
        history.save()
    except ValidationError as e:
        return Response({
            "success": False,
            "message": e.error_dict
        },
                        status=400)
    return Response({"success": True})


def clear_history(request):
    History.objects.filter(user=request.user).delete()
    return Response({"success": True})
