from django.urls import path
from history import views

urlpatterns = [
    path(r'', views.index, name='index'),
]
