import time
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView


class PoTayTokenObtainPairSerializer(TokenObtainPairSerializer):
    def create(self, validated_data):
        return super().create(self, validated_data)

    def update(self, instance, validated_data):
        return super().update(self, instance, validated_data)

    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)
        token['iss'] = "PoTay Team, Fasilkom UI"
        token['iat'] = int(time.time())
        token['name'] = user.first_name
        if user.last_name:
            token['name'] += " " + user.last_name
        token['email'] = user.email
        return token


class PoTayTokenObtainPairView(TokenObtainPairView):
    serializer_class = PoTayTokenObtainPairSerializer
