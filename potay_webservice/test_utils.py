import json
from django.contrib.auth.models import User
from django.test import Client


def generate_test_token(user=None, password=None, admin=False):
    if not user:
        if admin:
            user = User.objects.create_superuser(username='admin',
                                                 email='admin@localhost',
                                                 password='local')
        else:
            user = User.objects.create_user(username='local',
                                            email='local@localhost',
                                            password='local',
                                            is_active=True)

    response = Client().post('/account/login/',
                             data=json.dumps({
                                 'username': user.username,
                                 'password': password or "local",
                             }),
                             content_type='application/json')
    return {'HTTP_AUTHORIZATION': "Bearer " + response.data.get("access", "")}
