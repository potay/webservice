"""
Django settings for potay_webservice project.

Generated by 'django-admin startproject' using Django 2.1.5.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.1/ref/settings/
"""

import os
from datetime import timedelta
import dj_database_url
import environ

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))
BASE_DIR = os.path.dirname(PROJECT_ROOT)


def env(key, default=None):
    env_driver = os.environ.get
    if os.path.exists(os.path.join(BASE_DIR, '.env')):
        environ.Path(BASE_DIR)
        env_driver = environ.Env()
        environ.Env.read_env('.env')
    return env_driver(key, default=default)


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/

# You MUST DEFINE the environment variable SECRET_KEY.
SECRET_KEY = env('SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
PRODUCTION = env("DATABASE_URL") is not None
DEBUG = env("DEBUG", default="false").lower() == "true" or not PRODUCTION

# PoTay specific settings
HOMEPAGE_URL = env("HOMEPAGE_URL", "")
PREDICTOR_URL = env("PREDICTOR_URL", "")
TEMP_LINK_EXPIRE_AFTER = timedelta(hours=3)

ALLOWED_HOSTS = [
    "potay-api.azurewebsites.net", "api.tot.bio", "webservice", "api-instance",
    "gitlab.com", "gitlab.cs.ui.ac.id", "localhost", "127.0.0.1"
]
if PREDICTOR_URL:
    ALLOWED_HOSTS.append(PREDICTOR_URL.split("://")[-1])
if HOMEPAGE_URL:
    ALLOWED_HOSTS.append(HOMEPAGE_URL.split("://")[-1])
USE_X_FORWARDED_HOST = True

# Application definition

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'rest_framework',
    'account',
    'history',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'potay_webservice.urls'

TEMPLATES = []

WSGI_APPLICATION = 'potay_webservice.wsgi.application'

# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        'OPTIONS': {
            'timeout': 30,
        },
        'TEST': {
            'OPTIONS': {
                'timeout': 30,
            },
        }
    }
}

if env("DATABASE_URL"):
    DATABASES['default'] = dj_database_url.config()

# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME':
        'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Authentication

LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'

# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/

LANGUAGE_CODE = 'en-gb'
TIME_ZONE = 'Asia/Jakarta'
USE_I18N = False
USE_L10N = False
USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

MEDIA_URL = '/uploads/'
MEDIA_ROOT = env("UPLOADS_DIR", os.path.join(PROJECT_ROOT, 'uploads'))

# Email

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = env("EMAIL_HOST", "localhost")
EMAIL_HOST_USER = env("EMAIL_HOST_USER", "")
EMAIL_HOST_PASSWORD = env("EMAIL_HOST_PASSWORD", "")
EMAIL_PORT = int(env("EMAIL_PORT", 465))
EMAIL_ADDRESS = env("EMAIL_ADDRESS", "%s@%s" % (EMAIL_HOST_USER, EMAIL_HOST))
EMAIL_USE_SSL = True

# Security overrides

SECURE_BROWSER_XSS_FILTER = True
SECURE_CONTENT_TYPE_NOSNIFF = True

# Django REST Framework settings

REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': ('rest_framework.renderers.JSONRenderer', ),
    'DEFAULT_AUTHENTICATION_CLASSES':
    ('rest_framework_simplejwt.authentication.JWTAuthentication', ),
    'EXCEPTION_HANDLER':
    'potay_webservice.rest.potay_exception_handler'
}

# JWT settings for authentication

SIMPLE_JWT = {
    'ACCESS_TOKEN_LIFETIME': timedelta(days=1),
    'REFRESH_TOKEN_LIFETIME': timedelta(days=7),
    'ROTATE_REFRESH_TOKENS': True,
    'BLACKLIST_AFTER_ROTATION': True,
    'ALGORITHM': 'HS512',
    'SIGNING_KEY': SECRET_KEY,
    'VERIFYING_KEY': None,
    'AUTH_HEADER_TYPES': ('Bearer', 'JWT'),
    'USER_ID_FIELD': 'id',
    'USER_ID_CLAIM': 'user_id',
    'AUTH_TOKEN_CLASSES': ('rest_framework_simplejwt.tokens.AccessToken', ),
    'TOKEN_TYPE_CLAIM': 'token_type',
    'JTI_CLAIM': 'jti',
    'SLIDING_TOKEN_REFRESH_EXP_CLAIM': 'refresh_exp',
    'SLIDING_TOKEN_LIFETIME': timedelta(days=1),
    'SLIDING_TOKEN_REFRESH_LIFETIME': timedelta(days=7),
}
