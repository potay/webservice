"""potay_webservice URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.middleware.csrf import get_token
from django.urls import include, path
from rest_framework.decorators import api_view
from rest_framework.response import Response


@api_view(["GET"])
def get_csrf_token(request):
    token = get_token(request)
    return Response({'success': True, 'result': token})


urlpatterns = [
    path(r'csrf/', get_csrf_token, name='csrf'),
    path(r'account/', include(('account.urls', 'account'),
                              namespace='account')),
    path(r'history/', include(('history.urls', 'history'),
                              namespace='history')),
]
