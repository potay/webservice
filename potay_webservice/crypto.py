from base64 import urlsafe_b64decode, urlsafe_b64encode
from datetime import datetime, timedelta
from cryptography.fernet import InvalidToken
from cryptography.fernet import Fernet, MultiFernet
from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError


def create_fermat_key(key):
    if len(key) != 24:
        raise ValueError("Key must be 24 ASCII characters.")
    return urlsafe_b64encode(urlsafe_b64encode(key.encode()))


def generate_time_token(delta=timedelta(0)):
    timestamp = str(datetime.now() + delta).encode()
    return urlsafe_b64encode(timestamp).decode().replace("=", "")


def validate_time_token(token):
    normalized = token + "=" * (-len(token) % 4)
    raw_timestamp = urlsafe_b64decode(normalized).decode()
    timestamp = datetime.strptime(raw_timestamp, "%Y-%m-%d %H:%M:%S.%f")
    if datetime.now() > timestamp + settings.TEMP_LINK_EXPIRE_AFTER:
        raise ValidationError("Link has expired.")


def generate_forgot_password_token(user, time_delta=timedelta(0)):
    time_token = generate_time_token(delta=time_delta)
    key1 = Fernet(create_fermat_key(settings.SECRET_KEY[0:24]))
    key2 = Fernet(create_fermat_key(settings.SECRET_KEY[26:50]))
    key3 = Fernet(create_fermat_key(settings.SECRET_KEY[13:37]))
    raw_code = "%s#%s#%s" % (user.password, user.username, time_token)
    fernet = MultiFernet([key1, key2, key3])
    token = urlsafe_b64encode(fernet.encrypt(raw_code.encode())).decode()
    return token.replace("=", "")


def validate_forgot_password_token(token):
    enc = urlsafe_b64decode(token + "=" * (-len(token) % 4))
    key1 = Fernet(create_fermat_key(settings.SECRET_KEY[0:24]))
    key2 = Fernet(create_fermat_key(settings.SECRET_KEY[26:50]))
    key3 = Fernet(create_fermat_key(settings.SECRET_KEY[13:37]))
    fernet = MultiFernet([key1, key2, key3])
    try:
        code = fernet.decrypt(enc).decode().split("#")
        user = User.objects.get(username=code[1], password=code[0])
        validate_time_token(code[2])
        return user
    except InvalidToken:
        raise ValidationError("Invalid token string for Fernet decryption.")
