import re
from django.conf import settings
from django.core.mail import EmailMultiAlternatives


def render_template(text, **payload):
    for key in payload:
        text = text.replace("{{%s}}" % (key, ), payload[key])
    return re.sub(r"{{[\w_-]+}}", "Unknown", text)


def build_message(plain_path, html_path, **payload):
    with open(plain_path, "r") as f:
        plain = f.read()
    plain = render_template(plain, **payload)
    with open(html_path, "r") as f:
        html = f.read()
    html = render_template(html, **payload)
    return {"plain": plain, "html": html}


def generate_email(title, to, **contents):
    email = EmailMultiAlternatives(
        title,
        contents.get("plain"),
        settings.EMAIL_ADDRESS,
        [to],
        reply_to=[settings.EMAIL_ADDRESS],
    )
    email.attach_alternative(contents.get("html"), "text/html")
    return email
