FROM python:alpine

ENV CACHE_DIR=/var/cache
ENV UPLOADS_DIR=/var/uploads
ENV LOG_DIR=/var/log/webservice
ENV ACCESS_LOG=/var/log/webservice/access_main.log
ENV ERROR_LOG=/var/log/webservice/error_main.log
ENV DEBUG=false
VOLUME ["/var/log/webservice", "/var/cache", "/var/uploads"]

COPY pip.conf /etc/pip.conf

WORKDIR /opt/webservice
RUN mkdir -p /opt/webservice

COPY . .
RUN \
 apk add --no-cache postgresql-client libffi libc-dev binutils --repository=http://dl-cdn.alpinelinux.org/alpine/edge/main && \
 apk add --no-cache --virtual .build-deps gcc python3-dev libffi-dev musl-dev postgresql-dev --repository=http://dl-cdn.alpinelinux.org/alpine/edge/main && \
 python3 -m pip install -r requirements.txt --no-cache-dir && \
 apk --purge del .build-deps

RUN chmod +x /opt/webservice/start.sh
EXPOSE 80
ENTRYPOINT ["/opt/webservice/start.sh"]
