from datetime import date, timedelta
from django.conf import settings
from django.contrib.auth.models import User
from django.core import mail
from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status
from account.serializers import UserBaseSerializer, UserSerializer
from potay_webservice.crypto import generate_forgot_password_token
from potay_webservice.test_utils import generate_test_token


class JWTTokenTest(TestCase):
    def test_token_generate_success(self):
        superuser_token = generate_test_token(admin=True)
        self.assertNotEqual(superuser_token["HTTP_AUTHORIZATION"], "Bearer ")
        user_token = generate_test_token(admin=False)
        self.assertNotEqual(user_token["HTTP_AUTHORIZATION"], "Bearer ")

    def test_token_generate_failed_invalid_password(self):
        user = User.objects.create_superuser(username='admin',
                                             email='admin@localhost',
                                             password='local')
        superuser_token = generate_test_token(user=user, password="meng")
        self.assertEqual(superuser_token["HTTP_AUTHORIZATION"], "Bearer ")


class AccountSerializersUnitTest(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.data = {
            "username": "sergio.wijnaldum",
            "password": "WijnAAldum#Sergi@",
            "email": "sergio.wijnaldum@mancity.com",
            "is_staff": True,
            "is_superuser": True,
            "is_active": True
        }
        cls.user = User.objects.create_user(**cls.data)

    def test_user_base_serializer(self):
        serialized = UserBaseSerializer(instance=self.user)
        self.assertEqual(set(serialized.data.keys()),
                         set(['id', 'username', 'email']))
        self.assertEqual(serialized.data["id"], self.user.pk)
        self.assertEqual(serialized.data["username"], self.data["username"])
        self.assertEqual(serialized.data["email"], self.data["email"])

    def test_user_serializer(self):
        serialized = UserSerializer(instance=self.user)
        self.assertEqual(
            set(serialized.data.keys()),
            set([
                'id', 'username', 'email', 'is_active', 'is_staff',
                'is_superuser', 'last_login', 'date_joined'
            ]))
        self.assertEqual(serialized.data["is_active"], self.data["is_active"])
        self.assertEqual(serialized.data["is_staff"], self.data["is_staff"])
        self.assertEqual(serialized.data["is_superuser"],
                         self.data["is_superuser"])
        self.assertEqual(serialized.data["last_login"], None)
        self.assertEqual(
            date.fromisoformat(serialized.data["date_joined"].split("T")[0]),
            date.today())


class AccountCreateAPIUnitTest(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.url = "/account/create/"
        cls.payload_pos = {
            "username": "ann_spencer",
            "email": "stancey.spencer@gmail2.com",
            "password": "@stanceySpencer33",
        }
        cls.payload_neg = {
            "username": "spencer!!",
            "email": "meng",
            "password": "",
        }
        cls.payload_add = dict(cls.payload_pos)
        cls.payload_add.update({
            "is_staff": True,
            "is_superuser": True,
        })

    def assertRole(self, username, is_active=True, is_superuser=False):
        user = User.objects.filter(username=username).first()
        self.assertEqual(user.is_active, is_active)
        self.assertEqual(user.is_staff, is_superuser)
        self.assertEqual(user.is_superuser, is_superuser)

    def test_pub_create_user_success(self):
        # Positive test: public
        response = APIClient().post(self.url, self.payload_pos, format='json')
        self.assertTrue(response.data["success"])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertRole(self.payload_pos["username"])

    def test_pub_create_user_invalid(self):
        # Negative test: public, invalid input
        response = APIClient().post(self.url, self.payload_neg, format='json')
        self.assertFalse(response.data["success"])
        self.assertEqual(len(response.data["message"]), 2)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            User.objects.filter(username=self.payload_neg["username"]).count(),
            0)

    def test_pub_create_user_role_persists(self):
        # Negative test: insert is_staff=False, is_superuser=True
        response = APIClient().post(self.url, self.payload_add, format='json')
        self.assertTrue(response.data["success"])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertRole(self.payload_pos["username"])

    def test_reg_create_user_fail(self):
        # Negative test: registered user, must log out
        headers = generate_test_token(admin=False)
        response = APIClient().post(self.url,
                                    self.payload_pos,
                                    format='json',
                                    **headers)
        self.assertFalse(response.data["success"])
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            User.objects.filter(username=self.payload_pos["username"]).count(),
            0)


class AccountDeleteAPITest(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.url = "/account/delete/"

    def test_reg_delete_current_user_success(self):
        # Positive test: registered user
        headers = generate_test_token(admin=False)
        response = APIClient().delete(self.url, **headers)
        self.assertTrue(response.data["success"])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertFalse(User.objects.filter(username="local").exists())

    def test_pub_delete_current_user_fail(self):
        # Negative test: public
        response = APIClient().delete(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class AccountPasswordAPIUnitTest(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.base_url = "/account/password/"
        cls.payload = {
            "username": "ann_spencer",
            "email": "stancey.spencer@gmail2.com",
            "password": "@stanceySpencer33",
        }
        cls.user = User.objects.create_user(**cls.payload)
        cls.code = generate_forgot_password_token(cls.user)
        exp = -settings.TEMP_LINK_EXPIRE_AFTER - timedelta(1)
        cls.expired = generate_forgot_password_token(cls.user, time_delta=exp)

    def test_pub_forgot_password_success(self):
        # Positive test: public and email found
        response = APIClient().post(self.base_url + "forgot/",
                                    {"email": self.user.email},
                                    format='json')
        self.assertTrue(response.data["success"])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(mail.outbox), 1)
        self.assertIn(self.user.email, mail.outbox[0].to)

    def test_pub_forgot_password_email_not_found(self):
        # Positive test: public and email not found (still return success)
        response = APIClient().post(self.base_url + "forgot/",
                                    {"email": "a@meng.com"},
                                    format='json')
        self.assertTrue(response.data["success"])
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(mail.outbox), 0)

    def test_reg_forgot_password_fail(self):
        # Negative test: registered user
        header = generate_test_token(admin=False)
        response = APIClient().post(self.base_url + "forgot/",
                                    {"email": "local@localhost"},
                                    format='json',
                                    **header)
        self.assertFalse(response.data["success"])
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data["message"], "Log out first.")

    def test_pub_change_password_via_link_head_success(self):
        # Positive test: check availability (HEAD) success
        response = APIClient().head(self.base_url + self.code + "/")
        self.assertTrue(response.data["success"])
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_pub_change_password_via_link_head_not_found(self):
        # Negative test: check availability (HEAD) fail
        response = APIClient().head(self.base_url + "a" * 32 + "/")
        self.assertFalse(response.data["success"])
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        response = APIClient().head(self.base_url + self.expired + "/")
        self.assertFalse(response.data["success"])
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_pub_change_password_via_link_success(self):
        # Positive test: no matter what, change via link will succeed
        response = APIClient().post(self.base_url + self.code + "/", {
            "password": "c:0ngc:0ng",
            "confirm_password": "c:0ngc:0ng"
        })
        self.assertTrue(response.data["success"])
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_pub_change_password_via_link_confirm_not_match(self):
        # Negative test: change via link, confirm_password not match
        response = APIClient().post(self.base_url + self.code + "/", {
            "password": "c:0ngc:0ng",
            "confirm_password": "c:0ngc:0nh"
        })
        self.assertFalse(response.data["success"])
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_reg_change_password_via_link_failed(self):
        # Negative test: registered user
        header = generate_test_token(admin=False)
        response = APIClient().post(self.base_url + self.code + "/", {
            "password": "c:0ngc:0ng",
            "confirm_password": "c:0ngc:0nh"
        }, **header)
        self.assertFalse(response.data["success"])
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data["message"], "Log out first.")

    def test_reg_change_password_success(self):
        # Positive test: registered user
        header = generate_test_token(admin=False)
        response = APIClient().post(
            self.base_url + "change/", {
                "old_password": "local",
                "password": "m:3ngm:3ng",
                "confirm_password": "m:3ngm:3ng"
            }, **header)
        self.assertTrue(response.data["success"])
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_reg_change_password_old_password_mismatch(self):
        # Negative test: registered user not providing its old password
        header = generate_test_token(admin=False)
        response = APIClient().post(
            self.base_url + "change/", {
                "old_password": "meng",
                "password": "m:3ngm:3ng",
                "confirm_password": "m:3ngm:3ng"
            }, **header)
        self.assertFalse(response.data["success"])
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_pub_change_password_fail(self):
        # Negative test: public
        response = APIClient().post(
            self.base_url + "change/", {
                "old_password": "local",
                "password": "m:3ngm:3ng",
                "confirm_password": "m:3ngm:3ng"
            })
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
