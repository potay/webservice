from django.urls import path, re_path
from rest_framework_simplejwt.views import (TokenRefreshView, TokenVerifyView)
from potay_webservice.jwt import PoTayTokenObtainPairView
from account import views

urlpatterns = [
    path(r'create/', views.create_user, name='create-user'),
    path(r'password/forgot/',
         views.forgot_password,
         name='forgot-password-link'),
    path(r'password/change/', views.change_password, name='change-password'),
    re_path(r'^password/(?P<token>[a-zA-Z0-9_-]+)/$',
            views.change_password_via_link,
            name='change-password-via-link'),
    path(r'delete/', views.delete_current_user, name='delete-user'),
    path(r'login/',
         PoTayTokenObtainPairView.as_view(),
         name='token_obtain_pair'),
    path(r'refresh-token/', TokenRefreshView.as_view(), name='token_refresh'),
    path(r'verify-token/', TokenVerifyView.as_view(), name='token_verify'),
]
