import os
from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.contrib.auth.models import AnonymousUser
from django.contrib.auth.hashers import make_password
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from potay_webservice.crypto import (generate_forgot_password_token,
                                     validate_forgot_password_token)
from potay_webservice.email import build_message, generate_email


def build_forgot_password_email(to, **payload):
    title = "tot.bio - Reset Password Request"
    plain_path = os.path.join(settings.BASE_DIR, "mail_templates",
                              "forgot_password.txt")
    html_path = os.path.join(settings.BASE_DIR, "mail_templates",
                             "forgot_password.html")
    contents = build_message(plain_path, html_path, **payload)
    return generate_email(title, to, **contents)


def check_user_errors(user):
    errors = {}
    try:
        user.full_clean()
    except ValidationError as e:
        errors.update(e.error_dict)
    return errors


@api_view(["POST"])
def create_user(request):
    if not isinstance(request.user, AnonymousUser):
        return Response(
            {
                "success": False,
                "message": "Log out berfore creating new account."
            },
            status=403)
    payload = dict(request.data)
    payload.update({
        "password": make_password(payload["password"]),
        "is_staff": False,
        "is_superuser": False,
        "is_active": True,
    })
    user = User(**payload)
    errors = check_user_errors(user)
    if errors:
        return Response({"success": False, "message": errors}, status=400)
    user.save()
    return Response({"success": True})


@api_view(["POST"])
def forgot_password(request):
    if not isinstance(request.user, AnonymousUser):
        return Response({
            "success": False,
            "message": "Log out first."
        },
                        status=403)
    user = User.objects.filter(email=request.data.get("email", "")).first()
    if user is not None:
        token = generate_forgot_password_token(user)
        link = "/reset-password?token=" + token
        variables = {"username": user.username, "link": link}
        build_forgot_password_email(user.email, **variables).send()
    return Response({"success": True})


@api_view(["POST", "HEAD"])
def change_password_via_link(request, token):
    if not isinstance(request.user, AnonymousUser):
        return Response({
            "success": False,
            "message": "Log out first."
        },
                        status=403)
    try:
        user = validate_forgot_password_token(token)
    except (User.DoesNotExist, ValidationError):
        return Response({
            "success": False,
            "message": "Incorrect link."
        },
                        status=404)
    if request.method == "POST":
        return commit_change_password(user, request.data)
    return Response({"success": True})


@api_view(["POST"])
@permission_classes((IsAuthenticated, ))
def change_password(request):
    if not request.user.check_password(request.data["old_password"]):
        return Response(
            {
                "success": False,
                "message": "You must provide old password."
            },
            status=400)
    return commit_change_password(request.user, request.data)


def commit_change_password(user, data):
    if data["password"] != data["confirm_password"]:
        return Response(
            {
                "success": False,
                "message": "Confirm password not matched with password."
            },
            status=400)
    user.set_password(data["password"])
    return Response({"success": True, "message": "Please log in again."})


@api_view(["DELETE"])
@permission_classes((IsAuthenticated, ))
def delete_current_user(request):
    request.user.delete()
    return Response({"success": True})
