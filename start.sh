#!/bin/sh
set -e

ACCESS_LOG=${ACCESS_LOG:--}
ERROR_LOG=${ERROR_LOG:--}

# Check that the database is available
if [ -n "$DATABASE_URL" ]
    then
    url=`echo $DATABASE_URL | awk -F[@//] '{print $4}'`
    database=`echo $url | awk -F[:] '{print $1}'`
    port=`echo $url | awk -F[:] '{print $2}'`
    echo "Waiting for $database:$port to be ready"
    while ! pg_isready -h "$database" -p "$port" --quiet; do
        # Show some progress
        echo -n '.';
        sleep 1;
    done
    echo "$database is ready"
    # Give it another second.
    sleep 1;
else
    echo "Skipping database check (will use SQLite3) because no DATABASE_URL in env..."
fi

# Initialize database
python3 manage.py migrate

# Create log file if not exists
touch $LOG_DIR/webservice_main.log

# Start PoTay Webservice
echo "Starting PoTay Webservice"
exec gunicorn 'potay_webservice.wsgi' \
    --bind '0.0.0.0:80' \
    --access-logfile "$ACCESS_LOG" \
    --error-logfile "$ERROR_LOG"
